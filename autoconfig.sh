#!/bin/bash

echo "Attention, ce script écrase votre .profile"
echo "Continuer malgré tout ? Ctr+C pour quitter sinon taper sur une touche"
read
echo
echo " - Installation des paquets si besoin"

packages='bc'

which yum &>/dev/null && packagemanager=yum
which zypper &>/dev/null && packagemanager=zypper
which apt &>/dev/null && packagemanager=apt
which dnf &>/dev/null && packagemanager=dnf
sudo $packagemanager install -qy $packages &>/dev/null

echo " - Copie des fichiers"

 # Path of git repository
GIT_PATH="$(realpath ${0%/*})"

cp -r $GIT_PATH/.profile $GIT_PATH/.profile.d ~
sudo cp $GIT_PATH/usr/local/bin/check_* /usr/local/bin/

source $HOME/.profile
