#!/bin/bash

NORMAL='\033[00m'
ROUGE='\033[31m'
VERT='\033[32m'
grep -E "^$1.*rw," /proc/mounts >/dev/null 2>&1 && echo -e "${VERT}rw${NORMAL}" || echo -e "${ROUGE}ro${NORMAL}"

