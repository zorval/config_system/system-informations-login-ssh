 
#!/usr/bin/env bash

echo -e "
${GRAS}${SSLIGNE}${LBLEU}Système${NORMAL}

- OS               : $(profile_info_system --release)
- Noyau            : $(profile_info_system --kernel)
- Uptime           : $(profile_info_system --uptime)
- Charge           : $(/usr/local/bin/check_load --print)
- Util. mémoire    : $(profile_check_memory)
- Util. Swap       : $(profile_check_swap)
- Statut systemd   : $(test_systemd)
- Nb. de processus : $(profile_info_system --procs)
- Utilisateur(s)   : $(profile_info_system --users)

${GRAS}${SSLIGNE}${LBLEU}Réseau${NORMAL}

$(profile_info_system --ip)

${GRAS}${SSLIGNE}${LBLEU}Volumétrie${NORMAL}
"
if timeout 2s df -h 2>&1 >/dev/null;then

    volumetrie

else
    echo -e $GRAS$ROUGE"Erreur lors l'exécution de la commande df : timeout"$NORMAL
fi

echo
timeout 2s /usr/local/bin/check_service --print
echo