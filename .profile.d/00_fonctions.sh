#!/bin/bash

source /etc/os-release

# VARIABLES

df_option_base="--type=ext4 --type=ext2 --type=ext3 --type=xfs --type=zfs"
df_option_montage="--type=nfs --type=nfs4 --type=cifs --type=fuse.sshfs"

volumetrie() {

    printf "${GRAS}${GRIS}"
    df -h --output=source,fstype,used,size,pcent,target --type=ext4 --type=ext3                                 \
        --type=ext2 --type=xfs --type=nfs --type=nfs4 --type=cifs --type=fuse.sshfs                             \
        --type=fat --type=vfat --type=zfs | tail -n +2 | sort -r -n -k 5 |                                      \
        awk -v rouge=$ROUGE -v jaune=$JAUNE -v vert=$VERT -v normal=$NORMAL -v gras=$GRAS                       \
        'BEGIN {printf "%s %s %s %s %s %s %s %s %s %s\n", "Sys. de fichiers", "Type", "Utilisé", "Taille", gras, "Uti%", gras, "Monté sur", "Mode", normal} {uti_percent=$5 ; sub("%","",$5) ; $5=$5+0 ; cmd="$HOME/.profile.d/scripts/rw_ro.sh $1" ; cmd | getline mode ; close(cmd) ; printf "%s %s %s %s %s %s %s %s %s\n", $1, $2, $3, $4, ($5>85)?rouge:($5>70?jaune:vert), uti_percent, normal, $6, mode}' | column -t
}

profile_info_system() {

    source /etc/os-release

    case $1 in

        '--uptime')
            if [[ "$NAME" =~ Red\ Hat|Debian|Ubuntu ]]; then
                uptime --pretty
            else
                uptime
            fi
            ;;

        '--release')
            if [ $ID = "rhel" ];then
                echo $PRETTY_NAME
            elif [ $ID = "debian" ];then
                if [ -f /etc/debian_version ];then
                    echo "$PRETTY_NAME $(cat /etc/debian_version)"
                else
                    echo "$PRETTY_NAME"
                fi
            fi
            ;;

        '--procs')
            echo $(( $(ps --no-headers -e | wc --lines) - 1 ))
            ;;

        '--kernel')
            uname --kernel-name --kernel-release --machine
            ;;

        '--users')
            users | wc --words
            ;;

        '--ip')
            /sbin/ip -brief -color address | sort | column -t
            ;;
    esac
}

profile_check_memory() {

    mem_avail=$(awk '/^MemAvailable/ {print $2}' /proc/meminfo)
    mem_total=$(awk '/^MemTotal/ {print $2}' /proc/meminfo)
    mem_not_avail=$(( $mem_total - $mem_avail ))
    percent_mem_used=$(echo "$mem_not_avail / $mem_total * 100" | bc -l)
    warn=$(echo "$mem_total * 0.70" | bc -l)
    crit=$(echo "$mem_total * 0.90" | bc -l)

    if (( $(echo "$mem_not_avail > $crit" | bc -l) )); then
        COLOR=$ROUGE
    elif (( $(echo "$mem_not_avail > $warn" | bc -l) )); then
        COLOR=$JAUNE
    else
        COLOR=$VERT
    fi
    LANG=C printf "${COLOR}%2.1f%%${NORMAL} (%s/%s)"                    \
        "$percent_mem_used"                                             \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $mem_not_avail)" \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $mem_total)"
}

profile_check_swap() {

    swap_total=$(awk '/^SwapTotal/ {print $2}' /proc/meminfo)
    if (( $swap_total > 0 )); then
        swap_free=$(awk '/^SwapFree/ {print $2}' /proc/meminfo)
        swap_used=$(( $swap_total - $swap_free ))
        swap_percent=$(echo "$swap_used / $swap_total * 100" | bc -l)
    else
        swap_used=0
        swap_percent=0.0
    fi
    warn=$(echo "$swap_total * 0.20" | bc -l)
    crit=$(echo "$swap_total * 0.50" | bc -l)
    if (( $(echo "$swap_used > $crit" | bc -l) )); then
        COLOR=$ROUGE
    elif (( $(echo "$swap_used > $warn" | bc -l) )); then
        COLOR=$JAUNE
    else
        COLOR=$VERT
    fi
    LANG=C printf "${COLOR}%2.1f%%${NORMAL} (%s/%s)"                    \
        "$percent_mem_used"                                             \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $swap_used)" \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $swap_total)"
}

test_systemd(){

    SYSTEMCTL_OK="0 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'."

    SYSTEMCTL_STATE=$(systemctl --failed)
    if [ "$SYSTEMCTL_STATE" = "$SYSTEMCTL_OK" ]; then
       echo -e ${VERT}"OK"${NORMAL}
    else
       echo -e ${ROUGE}"FAILED"${NORMAL}
    fi

}
