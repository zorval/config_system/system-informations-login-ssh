# CHANGELOG

* v3.0
  * :rocket: Affichage amélioré au niveau des services
  * :new: prise en comptes des services à superviser dans le fichier **files/check_services**
  * :new: Ajout de services "spécifiques" possible dans **/etc/default/surcharge_check_service**

* v2.1
  * :rocket: Prise en compte du gestionnaire de paquet

* v2.0
  * :rocket: amélioration du lsb_release
  * :pencil: renommage des fonctions
  * :rocket: Rajout du NFS4
