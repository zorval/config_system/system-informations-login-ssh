[![Generic badge](https://img.shields.io/badge/code-bash-lightgrey.svg)](https://shields.io/)

[![Generic badge](https://img.shields.io/badge/Maintener-Quentin_LEJARD-lightgrey.svg)](https://framagit.org/valde/)
[![Generic badge](https://img.shields.io/badge/Developer-Alban_VIDAL-lightgrey.svg)](hhttps://framagit.org/alban.vidal)

## Fonctionne sur : ![Generic badge](https://img.shields.io/badge/Ubuntu-18+-orange.svg) ![Generic badge](https://img.shields.io/badge/Debian-9+-blue.svg) ![Generic badge](https://img.shields.io/badge/Debian-10-blue.svg) [![Generic badge](https://img.shields.io/badge/Redhat-7+-red.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/Suse-12+-green.svg)](https://shields.io/)

* Version de l'OS
* Version du noyau
* Uptime
* Les interfaces et leurs IP
* Les disques avec contrôle de la volumétrie
* la charge CPU
* la mémoire 
* la SWAP
* Le nombre de processus
* Le nombre d'utilisateurs présents sur la machine
* L'affichage de l'état de services en particulier (voir ci-dessous)
* Attention : le prompt sera remplacé pour afficher un état de la branche dans laquelle vous êtes (fichier .profile.d/02_git

## Configuration

Se rendre dans le dossier **files**, puis renseigner les services à superviser (un par ligne) dans ces deux fichiers

* **liste_service_standard** (pour un affichage dans la partie Services standards)
* **liste_service_specifique** (pour un affichage dans la partie Services spécifiques)

puis lancer la commande `./autoconfig.sh`

![Generic badge](https://img.shields.io/badge/build-95%25-green.svg)

- [ ] Les différents list-timers avec systemD
